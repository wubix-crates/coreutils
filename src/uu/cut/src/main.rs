#[macro_use]
extern crate uucore;

mod buffer;
mod ranges;
mod searcher;

mod cut;
uucore_procs::main!(cut); // spell-checker:ignore procs uucore
