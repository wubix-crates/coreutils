// This file is part of the uutils coreutils package.
//
// (c) Rolf Morel <rolfmorel@gmail.com>
// (c) kwantam <kwantam@gmail.com>
//     * substantial rewrite to use the `std::io::BufReader` trait
//
// For the full copyright and license information, please view the LICENSE
// file that was distributed with this source code.

// spell-checker:ignore (ToDO) SRes Newl

use futures::future::LocalBoxFuture;
use librust::io::{BufRead, BufReader, PollBufRead, Read, Write};
use librust::io::{PollRead, Result as IoResult};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

#[allow(non_snake_case)]
pub mod Bytes {
    use super::LocalBoxFuture;
    use librust::io::Write;

    pub trait Select {
        fn select<'a, W: Write + Unpin>(
            &'a mut self,
            bytes: usize,
            out: Option<&'a mut W>,
        ) -> LocalBoxFuture<'a, Selected>;
    }

    #[derive(PartialEq, Eq, Debug)]
    pub enum Selected {
        NewlineFound,
        Complete(usize),
        Partial(usize),
        EndOfFile,
    }
}

#[derive(Debug)]
pub struct ByteReader<R>
where
    R: Read,
{
    pub inner: BufReader<R>,
    newline_char: u8,
}

impl<R: Read + Unpin> ByteReader<R> {
    pub fn new(read: R, newline_char: u8) -> ByteReader<R> {
        ByteReader {
            inner: BufReader::with_capacity(4096, read),
            newline_char,
        }
    }
}

impl<R: Read + Unpin> PollRead for ByteReader<R> {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<IoResult<usize>> {
        Pin::new(&mut self.inner).poll_read(cx, buf)
    }
}

impl<R: Read + Unpin> ByteReader<R> {
    pub async fn consume_line(&mut self) -> usize {
        let mut bytes_consumed = 0;
        let mut consume_val;
        let newline_char = self.newline_char;

        loop {
            {
                // need filled_buf to go out of scope
                let filled_buf = match self.inner.fill_buf().await {
                    Ok(b) => {
                        if b.is_empty() {
                            return bytes_consumed;
                        } else {
                            b
                        }
                    }
                    Err(e) => crash!(1, "read error: {}", e),
                };

                if let Some(idx) = filled_buf.iter().position(|byte| *byte == newline_char) {
                    consume_val = idx + 1;
                    bytes_consumed += consume_val;
                    break;
                }

                consume_val = filled_buf.len();
            }

            bytes_consumed += consume_val;
            Pin::new(&mut self.inner).consume(consume_val);
        }

        Pin::new(&mut self.inner).consume(consume_val);
        bytes_consumed
    }
}

impl<R: Read + Unpin> self::Bytes::Select for ByteReader<R> {
    fn select<'a, W: Write + Unpin>(
        &'a mut self,
        bytes: usize,
        out: Option<&'a mut W>,
    ) -> LocalBoxFuture<'a, Bytes::Selected> {
        Box::pin(async move {
            enum SRes {
                Comp,
                Part,
                Newl,
            }

            use self::Bytes::Selected::*;

            let newline_char = self.newline_char;
            let (res, consume_val) = {
                let buffer = match self.inner.fill_buf().await {
                    Err(e) => crash!(1, "read error: {}", e),
                    Ok(b) => b,
                };

                let (res, consume_val) = match buffer.len() {
                    0 => return EndOfFile,
                    buf_used if bytes < buf_used => {
                        // because the output delimiter should only be placed between
                        // segments check if the byte after bytes is a newline
                        let buf_slice = &buffer[0..=bytes];

                        match buf_slice.iter().position(|byte| *byte == newline_char) {
                            Some(idx) => (SRes::Newl, idx + 1),
                            None => (SRes::Comp, bytes),
                        }
                    }
                    _ => match buffer.iter().position(|byte| *byte == newline_char) {
                        Some(idx) => (SRes::Newl, idx + 1),
                        None => (SRes::Part, buffer.len()),
                    },
                };

                if let Some(out) = out {
                    crash_if_err!(1, out.write_all(&buffer[0..consume_val]).await);
                }
                (res, consume_val)
            };

            Pin::new(&mut self.inner).consume(consume_val);
            match res {
                SRes::Comp => Complete(consume_val),
                SRes::Part => Partial(consume_val),
                SRes::Newl => NewlineFound,
            }
        })
    }
}
