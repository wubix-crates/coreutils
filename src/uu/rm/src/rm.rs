//  * This file is part of the uutils coreutils package.
//  *
//  * (c) Alex Lyon <arcterus@mail.com>
//  *
//  * For the full copyright and license information, please view the LICENSE
//  * file that was distributed with this source code.

// spell-checker:ignore (ToDO) bitor ulong

extern crate getopts;
extern crate walkdir;

use librust::fs;
use librust::io::{stderr, stdin, BufRead, Write};
use librust::path::{Path, PathExt};
use librust::println;
use librust::stream::StreamExt;
use std::collections::VecDeque;
use std::ops::BitOr;
use walkdir::{DirEntry, WalkDir};

#[derive(Eq, PartialEq, Clone, Copy)]
enum InteractiveMode {
    None,
    Once,
    Always,
}

struct Options {
    force: bool,
    interactive: InteractiveMode,
    #[allow(dead_code)]
    one_fs: bool,
    preserve_root: bool,
    recursive: bool,
    dir: bool,
    verbose: bool,
}

static NAME: &str = "rm";
static VERSION: &str = env!("CARGO_PKG_VERSION");

pub async fn uumain(args: impl uucore::Args) -> i32 {
    let args = args.collect_str();

    // TODO: make getopts support -R in addition to -r
    let mut opts = getopts::Options::new();

    opts.optflag(
        "f",
        "force",
        "ignore nonexistent files and arguments, never prompt",
    );
    opts.optflag("i", "", "prompt before every removal");
    opts.optflag("I", "", "prompt once before removing more than three files, or when removing recursively.  Less intrusive than -i, while still giving some protection against most mistakes");
    opts.optflagopt(
        "",
        "interactive",
        "prompt according to WHEN: never, once (-I), or always (-i).  Without WHEN, prompts always",
        "WHEN",
    );
    opts.optflag("", "one-file-system", "when removing a hierarchy recursively, skip any directory that is on a file system different from that of the corresponding command line argument (NOT IMPLEMENTED)");
    opts.optflag("", "no-preserve-root", "do not treat '/' specially");
    opts.optflag("", "preserve-root", "do not remove '/' (default)");
    opts.optflag(
        "r",
        "recursive",
        "remove directories and their contents recursively",
    );
    opts.optflag("d", "dir", "remove empty directories");
    opts.optflag("v", "verbose", "explain what is being done");
    opts.optflag("h", "help", "display this help and exit");
    opts.optflag("V", "version", "output version information and exit");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => crash!(1, "{}", f),
    };

    let force = matches.opt_present("force");

    if matches.opt_present("help") {
        println!("{} {}", NAME, VERSION).await;
        println!().await;
        println!("Usage:").await;
        println!("  {0} [OPTION]... [FILE]...", NAME).await;
        println!().await;
        println!("{}", opts.usage("Remove (unlink) the FILE(s).")).await;
        println!("By default, rm does not remove directories.  Use the --recursive (-r)").await;
        println!("option to remove each listed directory, too, along with all of its contents")
            .await;
        println!().await;
        println!("To remove a file whose name starts with a '-', for example '-foo',").await;
        println!("use one of these commands:").await;
        println!("rm -- -foo").await;
        println!().await;
        println!("rm ./-foo").await;
        println!().await;
        println!("Note that if you use rm to remove a file, it might be possible to recover").await;
        println!("some of its contents, given sufficient expertise and/or time.  For greater")
            .await;
        println!("assurance that the contents are truly unrecoverable, consider using shred.")
            .await;
    } else if matches.opt_present("version") {
        println!("{} {}", NAME, VERSION).await;
    } else if matches.free.is_empty() && !force {
        show_error!("missing an argument");
        show_error!("for help, try '{0} --help'", NAME);
        return 1;
    } else {
        let options = Options {
            force,
            interactive: {
                if matches.opt_present("i") {
                    InteractiveMode::Always
                } else if matches.opt_present("I") {
                    InteractiveMode::Once
                } else if matches.opt_present("interactive") {
                    match &matches.opt_str("interactive").unwrap()[..] {
                        "none" => InteractiveMode::None,
                        "once" => InteractiveMode::Once,
                        "always" => InteractiveMode::Always,
                        val => crash!(1, "Invalid argument to interactive ({})", val),
                    }
                } else {
                    InteractiveMode::None
                }
            },
            one_fs: matches.opt_present("one-file-system"),
            preserve_root: !matches.opt_present("no-preserve-root"),
            recursive: matches.opt_present("recursive"),
            dir: matches.opt_present("dir"),
            verbose: matches.opt_present("verbose"),
        };
        if options.interactive == InteractiveMode::Once
            && (options.recursive || matches.free.len() > 3)
        {
            let msg = if options.recursive {
                "Remove all arguments recursively? "
            } else {
                "Remove all arguments? "
            };
            if !prompt(msg).await {
                return 0;
            }
        }

        if remove(matches.free, options).await {
            return 1;
        }
    }

    0
}

// TODO: implement one-file-system (this may get partially implemented in walkdir)
async fn remove(files: Vec<String>, options: Options) -> bool {
    let mut had_err = false;

    for filename in &files {
        let file = Path::new(filename);
        had_err = match file.symlink_metadata().await {
            Ok(metadata) => {
                if metadata.is_dir() {
                    handle_dir(file, &options).await
                } else if is_symlink_dir(&metadata) {
                    remove_dir(file, &options).await
                } else {
                    remove_file(file, &options).await
                }
            }
            Err(_e) => {
                // TODO: actually print out the specific error
                // TODO: When the error is not about missing files
                // (e.g., permission), even rm -f should fail with
                // outputting the error, but there's no easy eay.
                if !options.force {
                    show_error!("no such file or directory '{}'", filename);
                    true
                } else {
                    false
                }
            }
        }
        .bitor(had_err);
    }

    had_err
}

async fn handle_dir(path: &Path, options: &Options) -> bool {
    let mut had_err = false;

    let is_root = path.has_root() && path.parent().is_none();
    if options.recursive && (!is_root || !options.preserve_root) {
        if options.interactive != InteractiveMode::Always {
            if let Err(e) = fs::remove_dir_all(path).await {
                had_err = true;
                show_error!("could not remove '{}': {}", path.display(), e);
            }
        } else {
            let mut dirs: VecDeque<DirEntry> = VecDeque::new();

            let mut stream = WalkDir::new(path).into_stream();
            while let Some(entry) = stream.next().await {
                match entry {
                    Ok(entry) => {
                        let file_type = entry.file_type();
                        if file_type.is_dir() {
                            dirs.push_back(entry);
                        } else {
                            had_err = remove_file(entry.path(), options).await.bitor(had_err);
                        }
                    }
                    Err(e) => {
                        had_err = true;
                        show_error!("recursing in '{}': {}", path.display(), e);
                    }
                }
            }

            for dir in dirs.iter().rev() {
                had_err = remove_dir(dir.path(), options).await.bitor(had_err);
            }
        }
    } else if options.dir && (!is_root || !options.preserve_root) {
        had_err = remove_dir(path, options).await.bitor(had_err);
    } else if options.recursive {
        show_error!("could not remove directory '{}'", path.display());
        had_err = true;
    } else {
        show_error!(
            "could not remove directory '{}' (did you mean to pass '-r'?)",
            path.display()
        );
        had_err = true;
    }

    had_err
}

async fn remove_dir(path: &Path, options: &Options) -> bool {
    let response = if options.interactive == InteractiveMode::Always {
        prompt_file(path, true).await
    } else {
        true
    };
    if response {
        match fs::remove_dir(path).await {
            Ok(_) => {
                if options.verbose {
                    println!("removed '{}'", path.display()).await;
                }
            }
            Err(e) => {
                show_error!("removing '{}': {}", path.display(), e);
                return true;
            }
        }
    }

    false
}

async fn remove_file(path: &Path, options: &Options) -> bool {
    let response = if options.interactive == InteractiveMode::Always {
        prompt_file(path, false).await
    } else {
        true
    };
    if response {
        match fs::remove_file(path).await {
            Ok(_) => {
                if options.verbose {
                    println!("removed '{}'", path.display()).await;
                }
            }
            Err(e) => {
                show_error!("removing '{}': {}", path.display(), e);
                return true;
            }
        }
    }

    false
}

async fn prompt_file(path: &Path, is_dir: bool) -> bool {
    if is_dir {
        prompt(&(format!("rm: remove directory '{}'? ", path.display()))).await
    } else {
        prompt(&(format!("rm: remove file '{}'? ", path.display()))).await
    }
}

async fn prompt(msg: &str) -> bool {
    let _ = stderr().write_all(msg.as_bytes()).await;
    let _ = stderr().flush().await;

    let mut buf = Vec::new();
    let stdin = stdin();
    let mut stdin = stdin.lock();

    match stdin.read_until(b'\n', &mut buf).await {
        Ok(x) if x > 0 => match buf[0] {
            b'y' | b'Y' => true,
            _ => false,
        },
        _ => false,
    }
}

#[cfg(not(windows))]
fn is_symlink_dir(_metadata: &fs::Metadata) -> bool {
    false
}

#[cfg(windows)]
use std::os::windows::prelude::MetadataExt;

#[cfg(windows)]
fn is_symlink_dir(metadata: &fs::Metadata) -> bool {
    use std::os::raw::c_ulong;
    pub type DWORD = c_ulong;
    pub const FILE_ATTRIBUTE_DIRECTORY: DWORD = 0x10;

    metadata.file_type().is_symlink()
        && ((metadata.file_attributes() & FILE_ATTRIBUTE_DIRECTORY) != 0)
}
