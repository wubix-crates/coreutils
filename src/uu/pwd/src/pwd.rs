//  * This file is part of the uutils coreutils package.
//  *
//  * (c) Derek Chiang <derekchiang93@gmail.com>
//  *
//  * For the full copyright and license information, please view the LICENSE
//  * file that was distributed with this source code.

extern crate getopts;

use librust::env;
use librust::io;
use librust::path::{Path, PathBuf, PathExt};

static NAME: &str = "pwd";
static VERSION: &str = env!("CARGO_PKG_VERSION");

pub async fn absolute_path(path: &Path) -> io::Result<PathBuf> {
    let path_buf = path.canonicalize().await?;

    #[cfg(windows)]
    let path_buf = Path::new(
        path_buf
            .as_path()
            .to_string_lossy()
            .trim_start_matches(r"\\?\"),
    )
    .to_path_buf();

    Ok(path_buf)
}

pub async fn uumain(args: impl uucore::Args) -> i32 {
    let args = args.collect_str();

    let mut opts = getopts::Options::new();

    opts.optflag("", "help", "display this help and exit");
    opts.optflag("", "version", "output version information and exit");
    opts.optflag(
        "L",
        "logical",
        "use PWD from environment, even if it contains symlinks",
    );
    opts.optflag("P", "physical", "avoid all symlinks");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => crash!(1, "Invalid options\n{}", f),
    };

    if matches.opt_present("help") {
        let msg = format!(
            "{0} {1}

Usage:
  {0} [OPTION]...

Print the full filename of the current working directory.",
            NAME, VERSION
        );
        librust::print!("{}", opts.usage(&msg)).await;
    } else if matches.opt_present("version") {
        librust::println!("{} {}", NAME, VERSION).await;
    } else {
        match env::current_dir().await {
            Ok(logical_path) => {
                if matches.opt_present("logical") {
                    librust::println!("{}", logical_path.display()).await;
                } else {
                    match absolute_path(&logical_path).await {
                        Ok(physical_path) => librust::println!("{}", physical_path.display()).await,
                        Err(e) => crash!(1, "failed to get absolute path {}", e),
                    };
                }
            }
            Err(e) => crash!(1, "failed to get current directory {}", e),
        };
    }

    0
}
