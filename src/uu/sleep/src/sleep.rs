//  * This file is part of the uutils coreutils package.
//  *
//  * (c) Alex Lyon <arcterus@mail.com>
//  *
//  * For the full copyright and license information, please view the LICENSE
//  * file that was distributed with this source code.

use librust::stream::{iter, StreamExt};
use librust::thread;
use librust::{print, println};
use std::time::Duration;

static NAME: &str = "sleep";
static VERSION: &str = env!("CARGO_PKG_VERSION");

pub async fn uumain(args: impl uucore::Args) -> i32 {
    let args = args.collect_str();

    let mut opts = getopts::Options::new();
    opts.optflag("h", "help", "display this help and exit");
    opts.optflag("V", "version", "output version information and exit");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            show_error!("{}", f);
            return 1;
        }
    };

    if matches.opt_present("help") {
        let msg = format!(
            "{0} {1}

Usage:
  {0} NUMBER[SUFFIX]
or
  {0} OPTION

Pause for NUMBER seconds.  SUFFIX may be 's' for seconds (the default),
'm' for minutes, 'h' for hours or 'd' for days.  Unlike most implementations
that require NUMBER be an integer, here NUMBER may be an arbitrary floating
point number.  Given two or more arguments, pause for the amount of time
specified by the sum of their values.",
            NAME, VERSION
        );
        print!("{}", opts.usage(&msg)).await;
    } else if matches.opt_present("version") {
        println!("{} {}", NAME, VERSION).await;
    } else if matches.free.is_empty() {
        show_error!("missing an argument");
        show_error!("for help, try '{0} --help'", NAME);
        return 1;
    } else {
        sleep(matches.free).await;
    }

    0
}

async fn sleep(args: Vec<String>) {
    let sleep_dur = iter(args)
        .fold(Duration::new(0, 0), |result, arg| async move {
            match uucore::parse_time::from_str(&arg[..]) {
                Ok(m) => m + result,
                Err(f) => crash!(1, "{}", f),
            }
        })
        .await;

    thread::sleep(sleep_dur).await;
}
