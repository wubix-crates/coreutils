//! stdio convenience fns

// spell-checker:ignore (ToDO) bslice

use librust::env;
use librust::io::{stderr, stdout, Write};
use librust::{print, writeln};

pub const EXIT_OK: i32 = 0;
pub const EXIT_ERR: i32 = 1;

pub async fn err_msg(msg: &str) {
    let exe_path = match env::current_exe().await {
        Ok(p) => p.to_string_lossy().into_owned(),
        _ => String::from(""),
    };
    writeln!(&mut stderr(), "{}: {}", exe_path, msg)
        .await
        .unwrap();
}

// by default stdout only flushes
// to console when a newline is passed.
#[allow(unused_must_use)]
pub async fn flush_char(c: char) {
    print!("{}", c).await;
    stdout().flush().await;
}
#[allow(unused_must_use)]
pub async fn flush_str(s: &str) {
    print!("{}", s).await;
    stdout().flush().await;
}
#[allow(unused_must_use)]
pub async fn flush_bytes(bslice: &[u8]) {
    stdout().write(bslice).await;
    stdout().flush().await;
}
