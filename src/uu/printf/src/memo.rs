//! Memo runner of printf
//! Takes a format string and arguments
//! 1. tokenize format string into tokens, consuming
//! any subst. arguments along the way.
//! 2. feeds remaining arguments into function
//! that prints tokens.

use itertools::put_back_n;
use std::vec::IntoIter;
use std::{cell::RefCell, iter::Peekable, rc::Rc};

use crate::cli;
use crate::tokenize::sub::Sub;
use crate::tokenize::token::{Token, Tokenizer};
use crate::tokenize::unescaped_text::UnescapedText;

pub struct Memo {
    tokens: Vec<Box<dyn Token>>,
}

async fn warn_excess_args(first_arg: &str) {
    cli::err_msg(&format!(
        "warning: ignoring excess arguments, starting with '{}'",
        first_arg
    ))
    .await;
}

impl Memo {
    pub async fn new(pf_string: &str, pf_args_it: Rc<RefCell<Peekable<IntoIter<String>>>>) -> Memo {
        let mut pm = Memo { tokens: Vec::new() };
        let mut tmp_token: Option<Box<dyn Token>>;
        let it = Rc::new(RefCell::new(put_back_n(
            pf_string.chars().collect::<Vec<_>>().into_iter(),
        )));
        let mut has_sub = false;
        loop {
            tmp_token = UnescapedText::from_it(Rc::clone(&it), Rc::clone(&pf_args_it)).await;
            if let Some(x) = tmp_token {
                pm.tokens.push(x);
            }
            tmp_token = Sub::from_it(Rc::clone(&it), Rc::clone(&pf_args_it)).await;
            if let Some(x) = tmp_token {
                if !has_sub {
                    has_sub = true;
                }
                pm.tokens.push(x);
            }
            let borrowed_it = &mut *it.borrow_mut();
            if let Some(x) = borrowed_it.next() {
                borrowed_it.put_back(x);
            } else {
                break;
            }
        }
        if !has_sub {
            let mut drain = false;
            if let Some(first_arg) = pf_args_it.borrow_mut().peek() {
                warn_excess_args(first_arg).await;
                drain = true;
            }
            if drain {
                loop {
                    // drain remaining args;
                    if pf_args_it.borrow_mut().next().is_none() {
                        break;
                    }
                }
            }
        }
        pm
    }
    pub async fn apply(&self, pf_args_it: Rc<RefCell<Peekable<IntoIter<String>>>>) {
        for tkn in self.tokens.iter() {
            tkn.print(Rc::clone(&pf_args_it)).await;
        }
    }
    pub async fn run_all(pf_string: &str, pf_args: Vec<String>) {
        let arg_it = Rc::new(RefCell::new(pf_args.into_iter().peekable()));
        let pm = Memo::new(pf_string, Rc::clone(&arg_it)).await;
        loop {
            if arg_it.borrow_mut().peek().is_none() {
                break;
            }
            pm.apply(Rc::clone(&arg_it)).await;
        }
    }
}
