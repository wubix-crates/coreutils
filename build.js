const { spawn } = require(`child_process`);
const path = require(`path`);
const { once } = require(`events`);

const DEFAULT_BUILD = [
    `basename`,
    `cat`,
    `chmod`,
    `cksum`,
    `comm`,
    `cut`,
    `echo`,
    `false`,
    `head`,
    `mkdir`,
    `printf`,
    `pwd`,
    `rm`,
    `rmdir`,
    `sleep`,
    `tac`,
    `true`,
];

const utilities =
    process.argv.length > 2 ? process.argv.slice(2) : DEFAULT_BUILD;

const main = async () => {
    console.log(`going to build:`, ...utilities);

    for (const utility of utilities) {
        console.log(`building ${utility}...`);
        const cwd = path.join(__dirname, `src/uu`, utility);
        const wasmPackBuild = spawn(`wasm-pack`, [`build`], {
            cwd,
            stdio: `inherit`,
        });
        await once(wasmPackBuild, `exit`);

        spawn(`npx`, [`gitlab:wubix-crates/pkg2wef`], { cwd });
        // do not await on purpose: `pkg2wef` isn't that core-intensive and may
        // run in parallel just fine
    }
};

main();
